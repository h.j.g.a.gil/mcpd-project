# MCPD

This is our repository for the CPD project.
Our project is to implement a domain specific language, inspired by SQL, deeply embedded in Haskell.

The __src__ folder contains all the code files to be considered :
* *AST.hs* contains the Abstract Syntax Tree
* *Parser.hs* contains all parsing functions
* *Main.hs* runs the project.
