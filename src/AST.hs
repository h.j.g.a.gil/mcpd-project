module AST where

import           Text.Megaparsec

type Name = String
data Type = NType String | Type Type Type deriving (Eq, Show)

newtype Program = Program [Statement] deriving (Eq, Show)

data Row = Row SourcePos Name [Term] deriving (Eq, Show)

data Function = Function Term [Expression] deriving (Eq, Show)

data Expression = EVar Term | AOp (Maybe Type) UnaryOp Expression | Lambda Type [Term] Expression | BOp (Maybe Type) BinaryOp Expression Expression | EFunction Function deriving (Eq, Show)

data Term = Var SourcePos (Maybe Type) Name | Const SourcePos Type Name deriving (Eq, Show)

data Statement = SRow Row | SFunction Function | Let Term Expression | Declaration Term deriving (Eq, Show)

data UnaryOp = Neg deriving (Eq, Show)

data BinaryOp =
      Times
    | Divide
    | Plus
    | Minus
    | GreatEqual
    | Great
    | LessEqual
    | Less
    | Equals
    | NotEquals
    | And
    | Or
    deriving (Eq, Show)
