module Parser where

import           AST

import           Control.Monad                  (ap, void)
import           Control.Monad.Combinators.Expr
import           Data.Bifunctor
import           Data.Void
import           Text.Megaparsec
import           Text.Megaparsec.Char
import qualified Text.Megaparsec.Char.Lexer     as L

type Parser = Parsec Void String

parseFromFile :: String -> String -> Either (ParseErrorBundle String Void) Program
parseFromFile = parse parseProgram

-- | Consume empty space, line and block comments
sc :: Parser ()
sc = L.space space1 lineCmnt blockCmnt
  where lineCmnt = L.skipLineComment "//"
        blockCmnt = L.skipBlockComment "/*" "*/"

-- | Consume all empty space after every token
lexeme :: Parser a -> Parser a
lexeme = L.lexeme sc

-- | Parse a single symbol
symbol :: String -> Parser String
symbol = L.symbol sc

-- | Parse something between `String'.

pbetween :: String -> String -> Parser a -> Parser a
pbetween a  b = between (symbol a) (symbol b)

parens :: Parser a -> Parser a
parens = pbetween "(" ")"

-- | Parse something between braces.
braces :: Parser a -> Parser a
braces = pbetween "{" "}"

-- | Parse something between brackets.
brackets :: Parser a -> Parser a
brackets = pbetween "[" "]"

-- | Parse an integer.
number :: Parser Term
number = do
    pos <- getSourcePos
    let inumber a b = Const pos (NType a) . show <$> b
    lexeme (inumber "Int" L.decimal <|> inumber "Float" L.float)

-- | Parse a semicolon.
semi :: Parser String
semi = symbol ";"

-- | Parse a comma.
comma :: Parser String
comma = symbol ","

-- | Parse a arrow.
arrow :: Parser String
arrow = symbol "->"

rword :: String -> Parser ()
rword w = (lexeme . try) (string w *> notFollowedBy alphaNumChar)

-- | List of reserved words, very small right now
rws :: [String]
rws = ["let","Row", "\\"]

identifier :: Parser Term
identifier = do
    pos <- getSourcePos
    Var pos Nothing <$> identifier'

cidentifier :: Type -> Parser Term
cidentifier n_type = do
    pos <- getSourcePos
    Const pos n_type <$> identifier'

-- | Parse an identifier
identifier' :: Parser String
identifier' = (lexeme . try) (brackets p <|> p >>= check)
  where
    p       = (:) <$> letterChar <*> many alphaNumChar
    check x = if x `elem` rws
                then fail $ "keyword " ++ show x ++ " cannot be an identifier"
                else return x

parseProgram :: Parser Program
parseProgram = Program <$> endBy stmt semi <* eof

stmt :: Parser Statement
stmt = tbldeclsttmt <|> letStmt

tbldeclsttmt :: Parser Statement
tbldeclsttmt = do
    var@(Var pos Nothing name) <- identifier
    tableStmt pos name <|> declStmt name <|> functionStmt var

term :: Parser Term
term = number <|> identifier

tableStmt :: SourcePos -> Name -> Parser Statement
tableStmt located name = do
    void (symbol "=>")
    rword "Row"
    tableContents <- braces (sepBy tableContent comma)
    return $ SRow (Row located name tableContents)

tableContent :: Parser Term
tableContent = do
    Var pos _ name <- identifier
    void (symbol "::")
    type_name  <- identifier'
    return $ Const pos (NType type_name) name

functionStmt :: Term ->Parser Statement
functionStmt var = SFunction <$> function var

function :: Term -> Parser Function
function fterm = do
    parameters <- parens (sepBy expression comma)
    return $ Function fterm parameters

expression :: Parser Expression
expression = lambda <|> pexpression

evarfunc :: Parser Expression
evarfunc = (identifier >>= ap ((<|>) . (EFunction <$>) . function) (return . EVar)) <|> EVar <$> number

parseType' :: Parser Type
parseType' = do
     xs <- parens (sepBy (parseType <|> NType <$> identifier') arrow)
     return $ foldr1 Type xs

parseType :: Parser Type
parseType = symbol "::" *> parseType'

parselambdaType :: Parser ([Term], Type)
parselambdaType = second (foldr1 Type) . unzip <$> parens (sepBy ((,) <$> identifier <*> parseType) comma)

pexpression :: Parser Expression
pexpression = makeExprParser evarfunc operators

operators :: [[Operator Parser Expression]]
operators =
  [ [Prefix (AOp Nothing Neg <$ symbol "-") ]
  , [ InfixL (BOp Nothing Times <$ symbol "*")
    , InfixL (BOp Nothing Divide <$ symbol "/") ]
  , [ InfixL (BOp Nothing Plus <$ symbol "+")
    , InfixL (BOp Nothing Minus <$ symbol "-") ]
  , [ InfixL (BOp Nothing GreatEqual <$ symbol ">=")
    , InfixL (BOp Nothing Great <$ symbol ">")
    , InfixL (BOp Nothing LessEqual <$ symbol "<=")
    , InfixL (BOp Nothing Less <$ symbol "<") ]
  , [ InfixL (BOp Nothing Equals  <$ symbol "==")
    , InfixL (BOp Nothing NotEquals <$ symbol "!=") ]
  , [ InfixL (BOp Nothing And <$ symbol "&&")]
  , [ InfixL (BOp Nothing Or <$ symbol "||") ]
  ]

lambda :: Parser Expression
lambda = do
    rword "\\"
    (vars, l_type) <- parselambdaType
    rword "->"
    Lambda l_type vars <$> expression

letStmt :: Parser Statement
letStmt = do
    rword "let"
    lterm <- identifier
    ntype <- parseType
    void (symbol "=")
    Let lterm <$> expression

declStmt :: Name -> Parser Statement
declStmt t_name = Declaration <$> cidentifier (NType t_name)
