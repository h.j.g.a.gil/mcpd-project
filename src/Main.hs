{-# LANGUAGE CPP                #-}
{-# LANGUAGE DataKinds          #-}
{-# LANGUAGE DeriveGeneric      #-}
{-# LANGUAGE FlexibleInstances  #-}
{-# LANGUAGE OverloadedStrings  #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE TypeOperators      #-}



module Main where

import           AST
import           Options.Generic
import           Parser
import           Text.Megaparsec

-- | The arguments to the program:
-- | --input is the file to use as input
data Opt w = Opt
  { input    :: w ::: String <?> "Query program"
  } deriving (Generic)

instance ParseRecord (Opt Wrapped)

instance ParseRecord (Opt Unwrapped)

deriving instance Show (Opt Unwrapped)

main :: IO ()
main = do
  opts <- unwrapWithHelp "queryy"
  parseOpts (opts :: (Opt Unwrapped, IO ()))

parseOpts :: (Opt Unwrapped, IO ()) -> IO ()
parseOpts (Opt input, _) = do
    content <- readFile input
    let result = parseFromFile input content
    case result of
         Left err -> putStr (errorBundlePretty err)
         Right xs -> print xs
